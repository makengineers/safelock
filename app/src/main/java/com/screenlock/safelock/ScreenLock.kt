package com.screenlock.safelock

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.app.Activity
import android.app.PendingIntent
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_fullscreen.*
import org.mindrot.jbcrypt.BCrypt
import java.util.*
import kotlin.system.exitProcess


class ScreenLock : Activity(), View.OnClickListener {
    private val suicidalBroadcastReceiver: SuicidalBroadcastReceiver = SuicidalBroadcastReceiver(this)
    private var devicePolicyManager: DevicePolicyManager? = null
    private var compName: ComponentName? = null
    private var close = false
    private val ButtonIds = intArrayOf(
        R.id.lbtn1,
        R.id.lbtn2,
        R.id.lbtn3,
        R.id.lbtn4,
        R.id.lbtn5,
        R.id.lbtn6,
        R.id.lbtn7,
        R.id.lbtn8,
        R.id.lbtn9,
        R.id.lbtn0
    )
    private var enteredDigits = ""
    private var pinHash: String? = null
    private var showGesturePopup = false
    var secretStack: Stack<Any> = Stack()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_fullscreen)

        ResetStack()

        this.window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        devicePolicyManager = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        compName = ComponentName(this, AdminSetter::class.java)


        val filter = IntentFilter("Authentication_result")
        registerReceiver(suicidalBroadcastReceiver, filter)

        pinHash = intent.getStringExtra("screenLockPINHash")
        showGesturePopup = intent.getBooleanExtra("useGesture", false)
        setListeners()
    }

    private fun setListeners() {
        lbtnTick.setOnClickListener(this)
        lbtnDelete.setOnClickListener(this)

        for (i in ButtonIds.indices) {
            val button: Button? = this.findViewById(ButtonIds[i])
            button?.setOnClickListener(this)
        }
    }

    override fun onPause() {
        super.onPause()
        if(!close){
            wholething.setBackgroundColor(Color.rgb(0, 153, 204))
            
            val intent = Intent(this, ScreenLock::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
            try {
                pendingIntent.send()
            } catch (e: PendingIntent.CanceledException) {
                e.printStackTrace()
            }
            //devicePolicyManager!!.lockNow()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(suicidalBroadcastReceiver)
    }

    override fun onClick(v: View?) {
        when (v) {
            lbtnTick -> {
                if(pinHash==null) {
                    close = true
                    this.finishAndRemoveTask()
                }
                else if (BCrypt.checkpw(dotText.text.toString(), pinHash)) {
                    close = true

                    if(showGesturePopup){
                        showPopup()
                    }
                    else {
                        sendPhoneUnlockedBroadcast()
                        sendUnlockedBroadcast()
                        this.finishAndRemoveTask()
                    }
                } else {
                    dotText.text = ""
                    enteredDigits = ""
                    Toast.makeText(this, "PIN incorrect", Toast.LENGTH_SHORT).show()
                }
            }
            lbtnDelete -> {
                if (enteredDigits.isNotEmpty())
                    enteredDigits = enteredDigits.substring(0, enteredDigits.length - 1)
                dotText.text = enteredDigits
            }
            emergency_call_button -> {
                //TODO
            }
            else -> { // Note the block
                if (enteredDigits.length >= 8) {
                    Toast.makeText(this, "Max 8 characters", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    enteredDigits += (v as Button).text.toString()
                }
                dotText.text = enteredDigits
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == 24 || keyCode == 25) {
            if (keyCode == secretStack.peek()) {
                secretStack.pop()
                if (secretStack.empty()) {
                    close = true
                    this.finishAndRemoveTask()
                }
            } else {
                ResetStack()
            }
        } else this.onPause()
        return true
    }

    private fun ResetStack() {
        secretStack = Stack()
        secretStack.push(25)
        secretStack.push(25)
        secretStack.push(24)
        secretStack.push(25)
        secretStack.push(24)
        secretStack.push(24)
    }

    fun KillYourself(authResult : Boolean) {
        val colorFrom = Color.rgb(0, 0x99, 0xcc)
        val duration = 250L
        if(authResult){
            val colorTo = Color.rgb(49, 168, 50)
            ObjectAnimator.ofObject(
                    wholething,
                    "backgroundColor",
                    ArgbEvaluator(),
                    colorFrom,
                    colorTo
                )
                .setDuration(duration)
                .start()

            wholething.invalidate()

            val handler = Handler()

            handler.postDelayed({ close = true; this.finishAndRemoveTask() }, 500)
        }
        else {
            val colorTo = Color.rgb(201, 55, 44)
            ObjectAnimator.ofObject(
                    wholething,
                    "backgroundColor",
                    ArgbEvaluator(),
                    colorFrom,
                    colorTo
                )
                .setDuration(duration)
                .start()
            wholething.invalidate()
        }
    }

    private fun showPopup() {
        val popup = GesturePopup(this )
        popup.show()
    }

    fun YesClicked() {
        sendUnlockedBroadcast()
        sendPhoneUnlockedBroadcast()
        close = true
        this.finishAndRemoveTask()
    }
    fun NoClicked() {
        sendPhoneUnlockedBroadcast()
        close = true
        this.finishAndRemoveTask()
    }

    private fun sendUnlockedBroadcast() {
        val intent = Intent()
        intent.action = "ACTION_UNLOCKED"
        this.sendBroadcast(intent)
    }

    private fun sendPhoneUnlockedBroadcast() {
        val intent = Intent()
        intent.action = "ACTION_PHONE_UNLOCKED"
        this.sendBroadcast(intent)
    }
}