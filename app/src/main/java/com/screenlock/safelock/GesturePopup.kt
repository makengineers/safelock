package com.screenlock.safelock

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Button


class GesturePopup(private val screenLockActivity : ScreenLock) : Dialog(screenLockActivity), View.OnClickListener {
    var yes: Button? = null
    var no: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.gesture_popup)
        yes = findViewById(R.id.yes_button)
        no = findViewById(R.id.no_button)
        yes!!.setOnClickListener(this)
        no!!.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.yes_button -> screenLockActivity.YesClicked()
            R.id.no_button -> screenLockActivity.NoClicked()
            else -> {
            }
        }
        dismiss()
    }

}

